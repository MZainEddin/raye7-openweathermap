require "rails_helper"

RSpec.describe HomeController, :type => :controller do
  render_views
  describe "responds to" do
    it "responds to html by default" do
      get :index
      expect(response.content_type).to eq "text/html"
    end

    it "responds with random weather for more than one city" do
      get :index
      expect(response.body).to match /(class="city_card.*)+/im
    end

    it "responds with weather for Cairo" do
      api_key = ENV['GOOGLE_API_KEY']
      gp = GooglePlacesI.new(api_key)
      result = gp.autocomplete('Cairo', types: '(cities)')
      place_id = result[0].place_id

      get :weather, params: {place_id: place_id}
      expect(response.content_type).to eq "text/html"
      expect(response.status).to eq 200
      expect(response.body).to match /Cairo/im
    end
  end
end
