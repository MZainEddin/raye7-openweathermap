require 'rails_helper'

RSpec.describe Owm, :type => :model do
  it "Set the right API key for Owm object" do
    api_key = ENV['OWM_API_KEY']
    owm = Owm.new(api_key)
    expect(owm.api_key).to eq(api_key)
  end

  it "Get weather for city by name" do
    api_key = ENV['OWM_API_KEY']
    owm = Owm.new(api_key)
    result = owm.by_city_name('Cairo', 'EG')
    expect(result["cod"]).to eq(200)
    expect(result["name"]).to eq("Cairo")
  end

  it "Get weather of city by coordinates" do
    api_key = ENV['OWM_API_KEY']
    owm = Owm.new(api_key)
    result = owm.by_geo_coordinates(30.05, 31.24)
    expect(result["cod"]).to eq(200)
    expect(result["coord"]).to be_kind_of(Hash)
    expect(result["coord"]["lat"]).to eq(30.05)
    expect(result["coord"]["lon"]).to eq(31.24)
  end

  it "Get weather of city by zip code" do
    api_key = ENV['OWM_API_KEY']
    owm = Owm.new(api_key)
    result = owm.by_zip_code(32951, 'US')
    expect(result["cod"]).to eq(200)
    expect(result["name"]).to eq("Palm Bay")
  end

  it "Caches weather result" do
    api_key = ENV['OWM_API_KEY']
    owm = Owm.new(api_key)
    result = owm.by_city_name('Cairo', 'EG')
    expect(result["cod"]).to eq(200)
    expect(result["name"]).to eq("Cairo")

    cache_key = ['owm', 'by_city_name', ['Cairo', 'EG']]
    cached_result = Rails.cache.fetch(cache_key)
    expect(cached_result).to be_kind_of(Hash)
    expect(result["cod"]).to eq(200)
    expect(result["name"]).to eq("Cairo")
  end
end
