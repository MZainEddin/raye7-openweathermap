require 'rails_helper'

RSpec.describe External, :type => :model do
  it "HTTParty is included and get request is working" do
    dumy_request = External.get('https://google.com')
    expect(dumy_request.code).to eq(200)
  end
end
