require 'rails_helper'

RSpec.describe GooglePlacesI, :type => :model do
  it "Set the right API key for Google Places object" do
    api_key = ENV['GOOGLE_API_KEY']
    gp = GooglePlacesI.new(api_key)
    expect(gp.api_key).to eq(api_key)
  end

  it "Autocomplete cities and countries" do
    api_key = ENV['GOOGLE_API_KEY']
    gp = GooglePlacesI.new(api_key)
    result = gp.autocomplete('Cai', types: '(cities)')
    expect(result.length).to be >  0
    expect(result[0].description).to match(/Cairo/)
  end

  it "Get details of city passed on place_id from autocomplete" do
    api_key = ENV['GOOGLE_API_KEY']
    gp1 = GooglePlacesI.new(api_key)
    result1 = gp1.autocomplete('Cai', types: '(cities)')
    place_id = result1[0].place_id

    gp2 = GooglePlacesI.new(api_key)
    result2 = gp2.details(place_id)
    expect(result2[:status]).to eq(0)
    expect(result2[:data]).to be_kind_of(Hash)
    expect(result2[:data]["geometry"]["location"]["lat"]).to be_kind_of(Float)
    expect(result2[:data]["geometry"]["location"]["lng"]).to be_kind_of(Float)
  end

  it "Passes wrong place_id return status 1" do
    api_key = ENV['GOOGLE_API_KEY']
    gp = GooglePlacesI.new(api_key)
    result = gp.details('adfasdf')
    expect(result[:status]).to eq(1)
  end

  it "Caches search result" do
    api_key = ENV['GOOGLE_API_KEY']
    gp = GooglePlacesI.new(api_key)
    gp.autocomplete('Cai', types: '(cities)')

    cache_key = ['google_places', 'autocomplete', ['Cai', types: '(cities)']]
    cached_result = Rails.cache.fetch(cache_key)
    expect(cached_result).to be_kind_of(Array)
  end

  it "Caches details result" do
    api_key = ENV['GOOGLE_API_KEY']
    gp1 = GooglePlacesI.new(api_key)
    result1 = gp1.autocomplete('Cai', types: '(cities)')
    place_id = result1[0].place_id

    gp2 = GooglePlacesI.new(api_key)
    result2 = gp2.details(place_id)

    cache_key = ['google_places', 'details', place_id]
    cached_result = Rails.cache.fetch(cache_key)
    expect(cached_result).to be_kind_of(Hash)
  end
end
