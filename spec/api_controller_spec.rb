require "rails_helper"

RSpec.describe ApiController, :type => :controller do
  describe "responds to" do
    it "responds to json by default" do
      get :find_location, :params => { :q => "Any Name" }
      expect(response.content_type).to eq "application/json"
    end

    it "responds with list of cities" do
      get :find_location, :params => { :q => "Cairo" }
      expect(response.status).to eq(200)
      result = JSON.parse(response.body)
      expect(result.size).to be > 0
    end

    it "responds with empty area if city not exist" do
      get :find_location, :params => { :q => "Any Name" }
      expect(response.status).to eq(200)
      result = JSON.parse(response.body)
      expect(result.size).to eq 0
    end
  end
end
