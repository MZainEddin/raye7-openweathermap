class ApiController < ApplicationController
  def find_location
    client = GooglePlacesI.new(ENV['GOOGLE_API_KEY'])
    cities = client.autocomplete(params[:q], types: '(cities)')
    render json: cities.map {|r| {id: r.place_id, text: r.description}}
  end
end
