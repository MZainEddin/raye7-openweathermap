class HomeController < ApplicationController
  def index
    @result = []

    # Get random uniq 4 cities
    cities = []
    while cities.length != 4
      cities << Faker::Address.state
      cities = cities.uniq
    end

    # Get weather for random cities
    owm_client = Owm.new(ENV['OWM_API_KEY'])
    cities.each do |city|
      tmp_result = owm_client.by_city_name(city)
      @result << tmp_result if tmp_result["cod"] == 200
    end
  end

  def weather
    gp_client = GooglePlacesI.new(ENV['GOOGLE_API_KEY'])
    result = gp_client.details(params[:place_id])
    if result[:status] == 0
      data = result[:data]

      city_name = data["name"]
      country_code = data["address_components"][-1]["short_name"]
      formated_name = data["formatted_address"]

      owm_client = Owm.new(ENV['OWM_API_KEY'])
      owm_result = owm_client.by_city_name(city_name, country_code)
      near_weather = false

      if owm_result["cod"] != 200
        near_weather = true
        lat = data["geometry"]["location"]["lat"]
        lon = data["geometry"]["location"]["lng"]
        owm_client = Owm.new(ENV['OWM_API_KEY'])
        owm_result = owm_client.by_geo_coordinates(lat, lon)
      end

      if owm_result["cod"] == 200
        if near_weather
          city_name = owm_result["name"]
          country_code = owm_result["sys"]["country"]
        end

        @data = {
          name: city_name,
          country_code: country_code,
          formated_name: formated_name,
          near_weather: near_weather,
          owm_weather: owm_result["main"],
          owm_wind: owm_result["wind"]
        }
      else
        @msg = "Couldn't found weather for city #{city_name}"
      end
    else
      @msg = "Couldn't get city details"
    end
    render layout: false
  end
end
