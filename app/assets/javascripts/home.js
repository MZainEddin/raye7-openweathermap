// Place all the behaviors and hooks related to the matching controller here.
// All this logic will automatically be available in application.js.

$(document).ready(function () {
  $('.location-ajax').select2({
    placeholder: 'Search for a repository',
    width: '100%',
    minimumInputLength: 2,
    theme: "bootstrap",
    ajax: {
      url: '/api/find_location',
      delay: 250,
      processResults: function (data) {
        return {
          results: data
        };
      }
    }
  });


  $('.location-ajax').change(function() {
    // $.ajax({
    //   url: "/home/weather",
    //   beforeSend: function( xhr ) {
    //     xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    //   }
    // })
    //   .done(function( data ) {
    //     if ( console && console.log ) {
    //       console.log( "Sample of data:", data.slice( 0, 100 ) );
    //     }
    //   });
    $('#owm_result').html('<div class="loader"></div>');
    var place_id = $(this).val();
    $.get("/home/weather", {
      place_id: place_id
    }, function(data){
      $('#owm_result').html(data).removeClass('loader');
    });
  });
});
