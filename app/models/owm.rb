class Owm < External
  base_uri 'api.openweathermap.org/data'

  attr_reader :api_key

  def initialize(api_key)
    @api_key = api_key
    @options = { query: { APPID: api_key, units: 'metric' } }
  end

  def by_city_name(city_name, country_code=nil)
    q = [city_name]
    q << country_code if country_code.present?
    @options[:query][:q] = q.join(',')
    cache_key = ['owm', 'by_city_name', q]
    result = Rails.cache.fetch(cache_key, expires_in: 1.hours) do
      self.class.get("/2.5/weather", @options).parsed_response
    end
    result
  end

  def by_city_id(id)
    @options[:query][:id] = id.to_i
    self.class.get("/2.5/weather", @options)
  end

  def by_geo_coordinates(lat, lon)
    @options[:query][:lat] = lat.to_f
    @options[:query][:lon] = lon.to_f
    cache_key = ['owm', 'by_geo_coordinates', lat, lon]
    result = Rails.cache.fetch(cache_key, expires_in: 1.hours) do
      self.class.get("/2.5/weather", @options).parsed_response
    end
    result
  end

  def by_zip_code(code, country_code=nil)
    zip = [code]
    zip << country_code if country_code.present?
    @options[:query][:zip] = zip.join(',')
    self.class.get("/2.5/weather", @options)
  end
end
