class GooglePlacesI < External
	base_uri 'https://maps.googleapis.com/maps/api/place'

	attr_reader :api_key

	def initialize(api_key)
		@api_key = api_key
		@options = { query: { key: api_key } }
	end

	def autocomplete(*args)
		cache_key = ['google_places', 'autocomplete', args]
		result = Rails.cache.fetch(cache_key, expires_in: 24.hours) do
			client = GooglePlaces::Client.new(@api_key)
			client.predictions_by_input(*args)
		end
		result
	end

	def details(place_id)
		@options[:query][:place_id] = place_id
		cache_key = ['google_places', 'details', place_id]
		result = Rails.cache.fetch(cache_key, expires_in: 24.hours) do
			self.class.get("/details/json", @options).parsed_response
		end
		if result["status"] == "OK"
			{status: 0, data: result["result"]}
		else
			{status: 1}
		end
	end
end
