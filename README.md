# Simple Weather App based on OpenWetherMap APIs

This README would normally document whatever steps are necessary to get the
application up and running.

* REQUIRED RUBY VERSION: >= 2.2.2
* REQUIRED RUBYGEMS VERSION: >= 1.8.11
* Database: No database, you can use SQLite3
* RSpec is used for BDD
* Application is simple so we are using `memory_store` for caching. We can use and configure redis or other technologies later if we will scale the application.

Just clone the repo and run

```
rails s
```

Then visit http://localhost:3000


More information visit our [Wiki](https://github.com/MustafaZain/Raye7-OpenWeatherMap/wiki) page
